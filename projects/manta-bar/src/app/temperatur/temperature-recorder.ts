import {Temperature} from "./interfaces/temperature";
import {HttpClient} from "@angular/common/http";
import {BrewingState} from "../prompt/interfaces/brewing-state";

export class TemperatureRecorder {
  /**
   * Progress of temperature
   */
  progress: Temperature[];

  /**
   * Brewing state that got recorded
   */
  brewState: number;

  /**
   * Starting timestamp
   */
  start: Date;

  /**
   * Ending timestamp
   */
  end: Date;

  /**
   * Toggle polling
   */
  polling: boolean;

  /**
   * Timestamp used last time to get temperature progress
   */
  lastTick: number;

  constructor(private httpClient: HttpClient) {
    this.brewState = -1;
    this.start = new Date();
    this.end = new Date();
    this.lastTick = this.start.valueOf();

    this.progress = [];

    this.polling = false;
  }

  /**
   * Set brewing state
   */
  private setCurrentState(){
    this.httpClient.get<BrewingState>("http://localhost:5000/brewing/state").subscribe(response => {
      this.brewState = response.state;
    })
  }

  /**
   * Start recording of temperatures
   */
  public startRecording() {
    this.setCurrentState();
    this.httpClient.get<number>("http://localhost:5000/time").subscribe(response => {

      // Timestamp format differs
      this.start = new Date(response * 1000);
      this.lastTick = this.start.valueOf()/1000;

      this.polling = true;
      this.poll();
      console.log("Recording started");
    })
  }

  /**
   * Polling
   */
  private poll() {
    if (this.polling) {
      this.getTemperature(this.lastTick);
      setTimeout(() => {
        this.poll();
      }, 10000)
    }
  }

  /**
   * Update temperature progress
   * @param since Starting timestamp of time span
   */
  private getTemperature(since: number) {
    this.lastTick = new Date().valueOf();

    // Timestamp format differs
    this.lastTick = (this.lastTick-(this.lastTick%1000))/1000;
    this.httpClient.get("http://localhost:5000/brewing/temperature/" + since).subscribe(response => {
      Object.entries(response).forEach(entry => {
        this.progress.push({temperature: entry[1] as number, timestamp: new Date(entry[0] as unknown as number * 1000)});

        // Need to change memory reference to trigger rerender of the chart
        this.progress = [...this.progress];
      });

    });
  }

  /**
   * Stop recording
   */
  public stopRecording() {
    console.log("Recording stopped")
    this.polling = false;
    this.end = new Date();
  }


}
