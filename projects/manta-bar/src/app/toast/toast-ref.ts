import {OverlayRef} from "@angular/cdk/overlay";

export class ToastRef {
  constructor(readonly overlay: OverlayRef) { }

  /**
   * Remove overlay
   */
  close() {
    this.overlay.dispose();
  }

  /**
   * Get position of toast
   * 0 if not shown anymore
   */
  getPosition() {
    if (this.overlay.overlayElement) {
      return this.overlay.overlayElement.getBoundingClientRect().bottom;
    }
    return 0;
  }
}
