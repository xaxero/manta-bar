import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BrewingState} from "./interfaces/brewing-state";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PromptService {

  constructor(private httpClient: HttpClient) { }

  currentState(): Observable<BrewingState>{
    return this.httpClient.get<BrewingState>("http://localhost:5000/brewing/state");
  }

  /**
   * Confirm stat
   * @param state State to confirm
   */
  confirmState(state: number) {
    const url = "http://localhost:5000/brewing/confirm/" + state;
    this.httpClient.post(url, {}).subscribe(value => console.log(value));
  }

  /**
   * Start brewing
   */
  startBrewing() {
    const url = "http://localhost:5000/brewing/start/" + "test";
    this.httpClient.post(url, {}).subscribe(value => console.log(value));
  }


}
