import { Component, OnInit } from '@angular/core';
import {Status} from "./enums/status.enum";
import {HttpClient} from "@angular/common/http";
import {catchError, of} from "rxjs";

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {

  status: Status = Status.OFFLINE;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.httpClient.get<any>("http://localhost:5000/alive").pipe(
      catchError(err => of(Status.OFFLINE))
    ).subscribe(
      value => this.status = Status.ToEnum(value)
    )
  }

}
