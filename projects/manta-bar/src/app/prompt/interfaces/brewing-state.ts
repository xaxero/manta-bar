export interface BrewingState{
  state: number,
  detail: {
    position: string
    step: string,
    type: string
  }
}
