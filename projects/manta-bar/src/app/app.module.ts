import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StatusComponent } from './status/status.component';
import { PromptComponent } from './prompt/prompt.component';
import { SettingsComponent } from './settings/settings.component';
import { ToastComponent } from './toast/toast.component';
import { OverlayModule} from "@angular/cdk/overlay";
import { TemperatureComponent } from './temperatur/temperature.component';
import { StartpageComponent } from './Views/startpage/startpage.component';
import { ChartComponent } from './temperatur/chart/chart.component';

@NgModule({
  declarations: [
    AppComponent,
    StatusComponent,
    PromptComponent,
    SettingsComponent,
    ToastComponent,
    TemperatureComponent,
    StartpageComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    OverlayModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
