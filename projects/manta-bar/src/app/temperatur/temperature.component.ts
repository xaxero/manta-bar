import {Component, HostListener, OnInit} from '@angular/core';
import {TemperatureRecorder} from "./temperature-recorder";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperatureComponent implements OnInit {
  /**
   * List of recorders
   */
  temperatureRecorders: TemperatureRecorder[] = [];



  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
  }

  /**
   * Listener for custom event
   */
  @HostListener('document:machineStateChanged', ['$event.detail.oldState', '$event.detail.newState'])
  record(oldState: number, newState: number) {
    if (this.mashingStarted(oldState, newState) || this.hopCookingStarted(oldState, newState)) {
      const recorder = new TemperatureRecorder(this.httpClient);
      recorder.startRecording();
      this.temperatureRecorders.push(recorder);
    } else if (this.mashingStopped(oldState, newState) || this.hopCookingStopped(oldState, newState)) {
      this.temperatureRecorders[this.temperatureRecorders.length-1].stopRecording();
    }
  }

  /**
   * Mashing started
   */
  private mashingStarted(oldState: number, newState: number): boolean {
    return String(oldState)[0] == "1" && String(newState)[0] == "2";
  }

  /**
   * Mashing stopped
   */
  private mashingStopped(oldState: number, newState: number): boolean {
    return String(oldState)[0] == "2" && String(newState)[0] == "3";
  }

  /**
   * Hop cooking started
   */
  private hopCookingStarted(oldState: number, newState: number): boolean {
    return String(oldState)[0] == "3" && String(newState)[0] == "4";
  }

  /**
   * Hop cooking stopped
   */
  private hopCookingStopped(oldState: number, newState: number): boolean {
    return String(oldState)[0] == "4" && String(newState)[0] == "5";
  }


}
