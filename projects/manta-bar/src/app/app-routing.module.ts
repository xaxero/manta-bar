import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SettingsComponent} from "./settings/settings.component";
import {PromptComponent} from "./prompt/prompt.component";
import {StartpageComponent} from "./Views/startpage/startpage.component";

const routes: Routes = [
  {
    path: "settings",
    component: SettingsComponent,
    pathMatch: 'full'
  },
  {
    path: "",
    component: StartpageComponent,
    pathMatch: "full"
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
