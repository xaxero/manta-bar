import {Component, HostListener, OnInit} from '@angular/core';
import {PromptService} from "./prompt.service";
import {Machine} from "./machine/machine";
import {BrewingState} from "./interfaces/brewing-state";
import {ToastService} from "../toast/toast.service";
import {Title} from "@angular/platform-browser";
import {ToastColor} from "../toast/enums/toast-color";

@Component({
  selector: 'app-prompt',
  templateUrl: './prompt.component.html',
  styleUrls: ['./prompt.component.css']
})
export class PromptComponent implements OnInit {
  /**
   * Brewery machine
   */
  machine: Machine = new Machine();

  /**
   * API Polling
   * Default = true
   */
  private loop: boolean = true;

  constructor(private promptService: PromptService, private toastService: ToastService, private title: Title) {
    this.updateLoop();
    this.updateState();
  }

  /**
   * Listen for document focus
   */
  @HostListener('document:visibilitychange', ['$event'])
  visibilitychange() {
    if (!document.hidden)
      this.clearTabName();
  }

  /**
   * Set default tab name
   */
  private clearTabName() {
    this.title.setTitle("Manta-bar")
  }

  /**
   * Set updated tab name
   */
  private tabUpdate() {
    this.title.setTitle("Manta-bar (1)")
  }

  ngOnInit(): void {
  }

  /**
   * Polling API for state updates
   */
  private updateLoop() {
    if (this.loop) {
      this.updateState();
      this.checkAbortConditions();
      setTimeout(() => {
        this.updateLoop()
      }, 2000)
    }
  }

  /**
   * Get state from machine
   */
  updateState() {
    this.promptService.currentState().subscribe(
      (value: BrewingState) => {

        if (value.state != this.machine.info.state) {

          const event = new CustomEvent(
            'machineStateChanged',
            {
              detail: {
                'oldState': this.machine.info.state,
                'newState' : value.state
              }
            }
          );
          document.dispatchEvent(event);
          this.machine.info = value;

          const toastColor = value.detail.type == "REQUEST" ? ToastColor.WARNING : ToastColor.SUCCESS;
          this.toastService.show({text: value.detail.step, color: toastColor});

          if (document.hidden) {
            this.tabUpdate();
          }
        }
      })
  }

  /**
   * Confirm current state
   */
  confirmState() {
    this.promptService.confirmState(this.machine.info.state);
    setTimeout(() => {
        this.updateState();
      },
      300);
  }

  /**
   * Start brewing
   */
  startBrewing() {
    this.promptService.startBrewing();
  }

  /**
   * Stop API Polling
   */
  private checkAbortConditions() {
    this.loop = this.machine.info.detail.step != "FINISHED";
  }

  /**
   *
   */
  confirmationNeeded(): boolean {
    return this.machine.info.detail.type == "REQUEST";
  }

  /**
   * True if machine is not brewing
   */
  isBrewing(): boolean {
    return this.machine.info.state != 100;
  }
}
