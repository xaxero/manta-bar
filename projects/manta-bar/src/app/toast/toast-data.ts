import {ToastColor} from "./enums/toast-color";

export class ToastData {
  /**
   * Text of toast
   */
  text: string = "";

  /**
   * Color of toast
   */
  color: ToastColor = ToastColor.SUCCESS;
}
