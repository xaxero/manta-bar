import {Injectable, Injector} from '@angular/core';
import {Overlay} from "@angular/cdk/overlay";
import {ComponentPortal, PortalInjector} from "@angular/cdk/portal";
import {ToastComponent} from "./toast.component";
import {ToastRef} from "./toast-ref";
import {ToastData} from "./toast-data";

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  /**
   * Last shown toast
   */
  lastToast!: ToastRef;

  constructor(private overlay: Overlay, private parentInjector: Injector) { }

  /**
   * Show toast
   * @param data Data to be shown
   */
  public show(data: ToastData): ToastRef {
    const overlayRef = this.overlay.create({

      // Place new toast below other active toasts
      positionStrategy: this.overlay.position().global().right().top(this.getLastPosition())
    });

    const toastRef = new ToastRef(overlayRef);
    this.lastToast = toastRef;

    // Inject data into new toast
    const injector = ToastService.getInjector(data, toastRef, this.parentInjector)
    const toastPortal = new ComponentPortal(ToastComponent, null, injector);
    overlayRef.attach(toastPortal);

    return toastRef;
  }

  /**
   * Get position of last toast
   * 0 if no other toast is active
   */
  getLastPosition(): string {
    if (this.lastToast) {
      return this.lastToast.getPosition() + 'px';
    } else {
      return 0 + 'px';
    }
  }

  /**
   * Data injector
   * Used this tutorial https://adrianfaciu.dev/posts/angular-toast-service/
   * @param data
   * @param toastRef
   * @param parentInjector
   * @private
   */
  private static getInjector(data: ToastData, toastRef: ToastRef, parentInjector: Injector) {
    const tokens = new WeakMap();
    tokens.set(ToastData, data);
    tokens.set(ToastRef, toastRef);

    return new PortalInjector(parentInjector, tokens);


  }
}
