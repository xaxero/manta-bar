import {Component, OnInit} from '@angular/core';
import {ToastData} from "./toast-data";
import {ToastRef} from "./toast-ref";
import {ToastColor} from "./enums/toast-color";

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css']
})
export class ToastComponent implements OnInit {
  constructor(readonly data: ToastData, readonly ref: ToastRef) { }

  /**
   * Remove toast after 2,5 sec
   */
  ngOnInit(): void {
    setTimeout(() => {
      this.close()
    }, 2500);
  }

  /**
   * Remove toast
   */
  close() {
    this.ref.close();
  }





}
