export interface Temperature {
  temperature: number;
  timestamp: Date;
}
