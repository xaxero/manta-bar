import {BrewingState} from "../interfaces/brewing-state";

export class Machine {
  info: BrewingState = {
    state: 0,
    detail: {
      position: "",
      step: "",
      type: ""
    }
  }
}
