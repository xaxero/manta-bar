import {AfterViewInit, Component, Input, OnChanges, OnInit} from '@angular/core';
import * as d3 from 'd3'
import {Temperature} from "../interfaces/temperature";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, AfterViewInit, OnChanges {
  @Input()
  id = 0;

  /**
   * Height of the chart
   */
  height = 600;

  /**
   * Width of the chart
   */
  width = 600;

  /**
   * Margin to all sided
   */
  margin = 50;

  /**
   * X Axis
   */
  xAxis!: any;

  /**
   * Y Axis
   */
  yAxis!: any;

  /**
   * SVG element
   */
  svg!: any;

  /**
   * Shown data
   */
  @Input()
  data: Temperature[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.initChart();
    this.initAxes();
    this.drawLine();
  }

  /**
   * Rerender chart on changes
   */
  ngOnChanges() {
    if (this.svg) {
      this.initAxes();
      this.drawLine();
    }
  }

  /**
   * Initialize chart
   */
  initChart() {
    this.svg = d3.select('svg#chart' + this.id).attr("width", this.width + this.margin * 2).attr("height", this.height + this.margin * 2).style("background", "white")
      .append("g")
      .attr("transform",
        "translate(" + this.margin + "," + this.margin + ")");
  }

  /**
   * Scale both axis
   */
  initAxes() {
    // x Axis
    let maxXValue = Math.max(...this.data.flat().map(x => x.timestamp.valueOf()));
    let minXValue = Math.min(...this.data.flat().map(x => x.timestamp.valueOf()));
    this.xAxis = d3.scaleTime().range([0, this.width]).domain([minXValue, maxXValue]);

    // Remove old chart
    d3.selectAll("#xaxis" + this.id).remove();
    d3.selectAll("#xlabel" + this.id).remove();

    this.svg.append("g")
      .attr("class", "x axis")
      .attr("id", "xaxis" + this.id)
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(this.xAxis));

    // Label
    this.svg.append("text")
      .attr("class", "x label")
      .attr("text-anchor", "end")
      .attr("id", "xlabel" + this.id)
      .attr("x", this.width)
      .attr("y", this.height - 6)
      .text("Time");


    // y Axis
    let maxYValue = Math.max(...this.data.flat().map(x => x.temperature));
    this.yAxis = d3.scaleLinear().range([this.height, 0]).domain([0, maxYValue * 1.1]);

    // Remove old chart
    d3.selectAll("#yaxis" + this.id).remove();
    d3.selectAll("#ylabel" + this.id).remove();

    this.svg.append("g")
      .attr("class", "y axis")
      .attr("id", "yaxis" + this.id)
      .call(d3.axisLeft(this.yAxis));

    // Label
    this.svg.append("text")
      .attr("class", "y label")
      .attr("text-anchor", "end")
      .attr("id" ,"ylabel" + this.id)
      .attr("y", "6")
      .attr("dy", ".75em")
      .attr("transform", "rotate(-90)")
      .text("Temperature");
  }

  /**
   * Draw line based on data
   * Removes all other lines in chart
   */
  drawLine() {
    const line = d3.line<Temperature>()
      .x((d: Temperature) => this.xAxis(d.timestamp.valueOf()))
      .y((d: Temperature) => this.yAxis(d.temperature))

    d3.selectAll("#line" + this.id).remove();

    this.svg.append('path')
      .datum(this.data)
      .attr("fill", "none")
      .attr("stroke", "steelblue")
      .attr("class", "line")
      .attr("id", "line" + this.id)
      .attr("stroke-width", 1.5)
      .attr('d', line);
  }

}
