export enum Status {
  ALIVE = "Alive",
  OFFLINE = "Offline"
}

export namespace Status {
  export function ToEnum(value: boolean): Status {
    switch (value) {
      case true:
        return Status.ALIVE;
      case false:
        return Status.OFFLINE;
      default:
        return Status.OFFLINE;
    }
  }
}



